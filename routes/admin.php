<?php

Route::group(['prefix'=>'admin'], function(){
	Route::resource('categories', 'CategoryController');
	Route::resource('products', 'ProductController');
	Route::get('view', 'HomeController@view')->name('view');
	Route::post('view/approved/{id}', 'HomeController@approved')->name('view.approved');
	Route::post('view/rejected/{id}', 'HomeController@rejected')->name('view.rejected');
});