<?php

Route::group(['prefix'=>'user'], function(){
	Route::resource('orders', 'UserController');
	Route::post('index', 'UserController@index');
	Route::get('cart', 'CartController@index')->name('carts.all');
	Route::post('cart/add/{id}', 'CartController@add')->name('carts.add');
	Route::get('cart/empty', 'CartController@empty')->name('carts.empty');
	Route::post('cart/update', 'CartController@update')->name('carts.update');
	Route::post('cart/remove', 'CartController@remove')->name('carts.remove');
	Route::post('cart/checkout', 'CartController@checkout')->name('carts.checkout');
	Route::post('transaction/cancelled/{id}', 'UserController@cancelled')->name('transaction.cancelled');
	Route::post('cart/add/{id}', 'CartController@add')->name('carts.add');
	Route::get('about', 'HomeController@about')->name('about');
	Route::get('contact', 'HomeController@contact')->name('contact');
	Route::get('gallery', 'HomeController@gallery')->name('gallery');
});