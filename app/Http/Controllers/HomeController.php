<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Order;
use App\OrderItem;
use App\Transaction;
use App\Status;
use App\Category;
use DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function about()
    {
        return view('user.about');
    }

    public function contact()
    {
        return view('user.contact');
    }
    public function gallery()
    {
        return view('user.gallery');
    }

    public function view()
    {
        $transaction=Transaction::all();
        return view('admin.transaction')->with('transactions', $transaction);
    }

    public function approved(Request $request, $id){
        $transaction = Transaction::findOrFail($id);

        $transaction->status_id=$request->input('status_id');
        $transaction->save();

        $records = DB::table('transactions')
            ->join('items', 'transactions.item_id', '=', 'items.id')
            ->select('transactions.item_id', 'items.name')
            ->get();

        return redirect()->back();
        // return view('user.transaction')->with('transaction', $transaction);
    }

    public function rejected(Request $request, $id){
        $transaction = Transaction::findOrFail($id);

        $transaction->status_id=$request->input('status_id');
        $transaction->save();

        $records = DB::table('transactions')
            ->join('items', 'transactions.item_id', '=', 'items.id')
            ->select('transactions.item_id', 'items.name')
            ->get();

        return redirect()->back();
        // return view('user.transaction')->with('transaction', $transaction);
    }

}
