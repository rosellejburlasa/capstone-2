<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Session;
use App\Transaction;
use App\Order;
use App\OrderDetail;
use Auth;

class CartController extends Controller
{
    public function index()
    {
        $cart = session()->get('cart');

        $item_ids = (!empty($cart)) ? array_keys($cart):[];
        $items = Item::find($item_ids);

        return view('user.cart.index', ['items'=>$items]);
    }

    public function add(Request $request, $id)
    {
        // get product id and quantity
        $quantity = $request->input('qty');
        $reserved_date = $request->input('reserve');
        $returned_date = $request->input('return');
        
        // check the session cart if exist
        if($request->session()->exists('cart.'.$id)) 
        {
            $currentQuantity = $request->session()->get('cart.'.$id);
            $request->session()->put('cart.'.$id, $quantity + $currentQuantity);
        } 
        else 
        {
            $request->session()->put('cart.'.$id, $quantity);
        }


        Session::flash('success', 'Product(s) added to cart');
        return redirect()->route('orders.index');
    }

    public function update(Request $request)
    {
        // get product id and quantity 
        $id = $request->input('item_id');
        $qty = $request->input('quantity');
        // return $id." ".$qty;

        // Remove the product 
        $request->session()->forget('cart.'. $id);

        // Add the product with the updated quantity
        $request->session()->put('cart.'.$id, $qty);

        Session::flash('success', 'Product(s) updated to cart');
        return redirect()->route('carts.all');

    }

    public function remove(Request $request)
    {
        $item_id = $request->input('item_id');
        $request->session()->forget('cart.'.$item_id);
        return redirect()->route('carts.all');
    }

    public function empty()
    {
        session()->forget('cart');
        return redirect()->route('carts.all');
    }

    public function checkout(Request $request)
    {
        if (\Auth::check())
        {
            $cart = session()->get('cart');
            $item_ids = array_keys($cart);
            $items = Item::find($item_ids);


            $order = new Order;
            $order->user_id = auth()->user()->id;
            $order->price = $request->input('totalPrice');
            $order->save();

      
            foreach($products as $product)
            {
                $orderDetails = new OrderDetail;
                $orderDetails->order_id = $order->id;
                $orderDetails->item_id = $item->id;
                $orderDetails->qty = $cart[$item->id];
                $orderDetails->save();
    
                $request->session()->forget('cart.'.$item->id);
            }
    
            Session::flash('success', 'Thank you for shopping!');
            return redirect()->route('home');
        } 
        else 
        {
            return redirect()->route('login');
        }
        
    }
}
