  <!-- Site footer -->
<!-- <img class="img-fluid mt-5 pt-4" src="{{ URL::to('/images/footer_cover.png') }}"> -->
<footer class="site-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-xs-12">
        <p class="copyright-text">Copyright &copy; 2020 All Rights Reserved by 
     <a href="#">Bubut Games</a>.
        </p>
      </div>

      <div class="col-12 list-group list-group-horizontal">

            <span class="list-group-item mr-1">
              <a class="facebook" href="#"><i class="fa fa-facebook fa-lg"></i></a>
            </span>
            <span class="list-group-item mr-1">
              <a class="twitter" href="#"><i class="fa fa-twitter fa-lg"></i></a>
            </span>
            <span class="list-group-item mr-1">
              <a class="dribbble" href="#"><i class="fa fa-dribbble fa-lg"></i></a>
            </span>
            <span class="list-group-item mr-1">
              <a class="linkedin" href="#"><i class="fa fa-linkedin fa-lg"></i></a>
            </span>

      </div>
    </div>
  </div>
</footer>
