@extends('layouts.custom')

@section('content')
<div class="container mt-5 pt-2 mb-2">
	<div class="row">
		<div class="col">
			@can('isAdmin')
			<div class="card mb-3" style="max-width: 70rem;">
			    <div class="row no-gutters">
			        <div class="col-md-4">
			            <img src="{{$items->featured}}" style="height:450px; width:350px;" class="m-3">
			        </div>
			        <div class="col-md-8">
			            <div class="card-body">
			                <h2 style="font-weight: bold; color: orange;">{{$items->name}}</h2>
			                <h5><small>In stock: {{$items->stock}}</h5>
			                <h5>₱ {{number_format($items->price, 2)}}</h5>
			                <h6><strong>Product Description: </strong></h6>
			                <p>Includes: {{$items->includes}}</p>
			                <p>No. of Players: {{$items->player}}</p>
			                <p>{{$items->age}}</p>
			                <p>{{$items->description}}</p>		
			            </div>
			            <div class="pull-right ml-3 mb-5">
			                <a class="btn btn-outline-primary" href="{{ route('orders.index') }}"> Back</a>
			            </div>
			        </div>
			    </div>
			</div>
			@else
			<div class="card mb-3" style="max-width: 70rem;">
			    <div class="row no-gutters">
			        <div class="col-md-4">
			            <img src="{{$items->featured}}" style="height:450px; width:350px;" class="m-3">
			        </div>
			        <div class="col-md-8">
			            <div class="card-body">
			                <h2 style="font-weight: bold; color: orange;">{{$items->name}}</h2>
			                <h5><small>In stock: {{$items->stock}}</h5>
			                <h5>₱ {{number_format($items->price, 2)}}</h5>
			                <h6><strong>Product Description: </strong></h6>
			                <p>Includes: {{$items->includes}}</p>
			                <p>No. of Players: {{$items->player}}</p>
			                <p>{{$items->age}}</p>
			                <p>{{$items->description}}</p>
			               	<form method="POST" action="{{route('carts.add', ['id' => $items->id])}}">
							@csrf
								<input type="number" class="mb-3 mr-2 form-control text-center w-25" value="1" min="1" max="3" name="qty">
								<button class="btn btn-primary br-lg" type="submit">
									<i class="fas fa-cart-plus"></i> Reserve Now!
								</button>
							</form>				
			            </div>
			            <div class="pull-right ml-3 mb-5">
			                <a class="btn btn-outline-primary" href="{{ route('orders.index') }}"> Back</a>
			            </div>
			        </div>
			    </div>
			</div>
			@endcan
		</div>
	</div>
</div>		
@endsection
