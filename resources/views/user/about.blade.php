@extends('layouts.custom')
@section('content')
<!-- Back to top button -->
<a id="button"></a>
<div class="container my-5 py-5">
	<div class="row">
		<div class="col-lg-12">
			<div class="jumbotron bg-cover text-white" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 100%), url(https://images.pexels.com/photos/776654/pexels-photo-776654.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260)">
				<div class="container">
				  	<p class="lead text-center display-1">Who we are?</p>
				</div>
			</div> 
		</div>
	</div>
	<div class="row">
		<div class="col mb-5 pb-5">
			<section class="text-center about">
		      	<div class="container">
			        <div class="row">
			          	<div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200" >
				            <a href="#owner"><span class="fa fa-group"></span>
				            <h2>About Us!</h2></a>
			          	</div>
			          	<div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
				            <a href="{{route('contact')}}"><span class="fa fa-info"></span>
				            <h2>Contact Us!</h2></a>
			          	</div>
			          	<div class="clearfix visible-md-block visible-sm-block"></div>
			          	<div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
				            <a href="{{route('gallery')}}"><span class="fa fa-file"></span>
				            <h2>Photo Gallery!</h2></a>
			          	</div>     
			        </div>
		      	</div>
		    </section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center" id="owner">
			<img class="img-fluid h-75 mt-5" src="{{ URL::to('/images/aboutme.jpg') }}">
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<img class="img-fluid h-50 " src="{{ URL::to('/images/profile_img.jpg') }}" style="border-radius: 50%;">
		</div>
		<div class="col-lg-6">
			<p class="lead">
				Roselle Burlasa is a self proclaimed nerd/geek, and opening a book shop has fulfilled one of Roselle’s lifelong dreams. Roselle started reading at the age of two, earning the nickname “Johnny 5” after the Short Circuit robot who loved “input” any way he could get it. Besides books, Roselle loves board games, Hamilton: The Musical, Star Wars, Supernatural, Doctor Who, Thundercats, He-Man, She-Ra, Jem and the Holograms (see the 80s cartoon trend here?), Harry Potter, Game of Thrones, musical theater, and Disney, among other geeky things. Roselle also loves to write and tends to overachieve and get way too involved in things. Stop by, say hello, and ask for recommendations!
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center">
			<blockquote class="blockquote">
			  	<p class="mb-0">We don’t stop playing because we grow old; We grow old because we stop playing.</p>
			  	<footer class="blockquote-footer">George Bernard Shaw</footer>
			</blockquote>
		</div>
	</div>
</div>
@endsection