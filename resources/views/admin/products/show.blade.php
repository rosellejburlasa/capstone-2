@extends('layouts.custom')

@section('content')
<div class="container my-5 py-5">
	<div class="row">
		<div class="col">
			<div class="card mb-3 mt-5" style="max-width: 70rem;">
			    <div class="row no-gutters">
			        <div class="col-md-4">
			            <img src="{{$item->featured}}" style="height:300px; width:350px;" class="m-3">
			        </div>
			        <div class="col-md-8">
			            <div class="card-body">
			                <h2>{{$item->name}}</h2>
			                <p><strong>Product Description: </strong></p>
			                <p>Includes: <br>{{$item->includes}}</p>
			                <p>{{$item->description}}</p>
				          	<br>
			            </div>
			            <div class="pull-right ml-3 mb-5">
			                <a class="btn btn-outline-primary" href="{{ route('products.index') }}"> Back</a>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>		
@endsection
