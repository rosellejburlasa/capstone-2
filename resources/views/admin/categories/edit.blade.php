@extends('layouts.custom')

@section('content')
<div class="container my-5 pt-5">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			@if(count($errors) > 0)
				<ul class="list-group">
					@foreach($errors->all() as $error)
						<li class="list-group-item text-danger">
							{{$error}}
						</li>
					@endforeach
				</ul>
			@endif
			<br>
			<div class="card">
				<div class="card-header">
					Update Category: {{$category->name}}
				</div>
				<div class="card-body">
					{{-- Form for creating a post --}}
					<form method="POST" action="{{ route('categories.update',$category->id) }}">
						@csrf
						@method('PUT')
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" class="form-control" value="{{$category->name}}">
						</div>
						<div class="form-group">
							<button class="btn btn-primary" type="submit">Update Category</button>
						</div>
						<a class="btn btn-outline-primary" href="{{route('categories.index')}}"> Back</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection