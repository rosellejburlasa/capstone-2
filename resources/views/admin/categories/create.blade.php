@extends('layouts.custom')

@section('content')
	<div class="container my-5 py-5">
		<div class="row">
			<div class="col-lg-8 offset-lg-2 mt-2">
				@if ($errors->any())
			    <div class="alert alert-danger">
			        <strong>Whoops!</strong> There were some problems with your input.<br><br>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
				<br>
				<div class="card mb-5">
					<div class="card-header" style="background-color: orange;">
						<h4><strong>Create a Category</strong></h4>
					</div>
					<div class="card-body">
						{{-- Form for creating a post --}}
						<form method="post" action="{{route('categories.store')}}">
							@csrf
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control">
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Submit Category</button>
							</div>
							<a class="btn btn-outline-primary" href="{{route('categories.index')}}"> Back</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection