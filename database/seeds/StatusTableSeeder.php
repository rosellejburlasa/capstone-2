<?php

use Illuminate\Database\Seeder;
use App\Status;
class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        DB::table('statuses')->insert(
            [
                [
                    'name'=> 'Pending',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ],
                [
                    'name'=> 'Cancelled',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ],
                [
                    'name'=> 'Approved',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ],
                [
                    'name'=> 'Rejected',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ],
                [
                    'name'=> 'Returned',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ]
            ]
        );
    }
}
