<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name'=>'Rozelda Burlasa',
            'address'=>'Mandaluyong, Philippines',
        	'email'=>'demo@gmail.com',
        	'password'=>bcrypt('12345678'),
        	'role'=>'user'
        ]);

        DB::table('users')->insert([
        	'name'=>'Roselle Burlasa',
            'address'=>'Imus Cavite, Philippines',
        	'email'=>'admin@gmail.com',
        	'password'=>bcrypt('12345678'),
        	'role'=>'admin'
        ]);
    }
}
